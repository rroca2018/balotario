import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReporteComponent } from './components/reporte/reporte.component';
import { AuthRouting } from '@shared/auth/auth-routing';
import { ResultadoComponent } from './components/resultado/resultado.component';
import { UsuarioComponent } from './components/administrador/usuario/usuario.component';

const routes: Routes = [
  { path: 'reporte', component: ReporteComponent, canActivate: [AuthRouting] },
  { path: 'resultado', component: ResultadoComponent, canActivate: [AuthRouting] },
  { path: 'bandeja-usuario', component: UsuarioComponent, canActivate: [AuthRouting]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BalotarioRoutingModule { }
