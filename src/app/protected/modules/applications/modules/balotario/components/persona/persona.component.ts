import { Component, OnInit } from '@angular/core';
import { BalotarioService } from '../../service/balotario.service';
import { WsResponse } from '../../dto/WsResponse';
import { preguntaResponse } from '../../dto/preguntaResponse';
import { Cuestionario } from '../../dto/Cuestionario';
import { Session } from '@shared/auth/Session';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.scss'],
  providers: [BalotarioService]
})
export class PersonaComponent implements OnInit {

  numeroPregunta: number = 1;
  cantidadPregunta: number = 0;
  totalPregunta: number;
  porcentaje: number;
  bloqueo: boolean = true;

  pregunta: preguntaResponse[];
  iteracion: number = 0;
  estadoCorrecto: boolean;
  idPregunta: number;

  resultado: any[] = [];
  calificacion: boolean = (Cuestionario.identityCalification == true) ? true : false;

  cronometro: boolean;

  contadorPreguntaCorrecta: number = 0;
  contadorPreguntaIncorrecta: number = 0;

  resultCalificacion: number;
  numeroIntento: number;
  cantidadIntento: number;
  tiempo: string;

  mostrarInicio: boolean = false;

  constructor(
    private balotarioService: BalotarioService,
  ) { }

  ngOnInit() {
    if (localStorage.getItem('init') == 'true') {
      this.mostrarInicio = true;
    }
    this.tiempo = Session.identity.cantidad_tiempo;
    this.cantidadPregunta = Session.identity.cantidad_pregunta;
    this.cantidadIntento = Session.identity.cantidad_intento;
    this.cargarBalotario(this.cantidadPregunta);
  }

  response(cuestionario) {
    this.idPregunta = cuestionario.idPregunta;
    this.estadoCorrecto = cuestionario.correcto;
    this.bloqueo = cuestionario.estado;
  }

  async intentaNuevo(intento) {
    Cuestionario.startIteration(Object.assign({}, Cuestionario.identityIteration, ''));
    Cuestionario.startQuestion(Object.assign({}, Cuestionario.identityNumberQuestion, ''));
    Cuestionario.startCalification(Object.assign({}, Cuestionario.identityCalification, ''));
    Cuestionario.startResultCalificacion(Object.assign({}, Cuestionario.identityResultCalificacion, ''));
    Cuestionario.startResultado(Object.assign({}, Cuestionario.identityResult, ''));
    Cuestionario.startReloaded(Object.assign({}, Cuestionario.identityReloaded, ''));
    await this.cargarBalotario(this.cantidadPregunta);
    this.bloqueo = true;
    this.calificacion = false;
  }

  async finalizaCuestionario(finaliza) {
    Cuestionario.startIteration(Object.assign({}, Cuestionario.identityIteration, ''));
    Cuestionario.startQuestion(Object.assign({}, Cuestionario.identityNumberQuestion, ''));
    Cuestionario.startCalification(Object.assign({}, Cuestionario.identityCalification, ''));
    Cuestionario.startResultCalificacion(Object.assign({}, Cuestionario.identityResultCalificacion, ''));
    Cuestionario.startResultado(Object.assign({}, Cuestionario.identityResult, ''));
    Cuestionario.startReloaded(Object.assign({}, Cuestionario.identityReloaded, ''));
    localStorage.setItem('#nI', (1).toString());
    await this.cargarBalotario(this.cantidadPregunta);
    localStorage.setItem('init', null);
    this.mostrarInicio = false;
    this.calificacion = false;
    this.bloqueo = true;
  }

  terminarTiempo(terminaTiempo) {
    this.calificacion = terminaTiempo;
    this.contadorPreguntaCorrecta = 0;
    this.contadorPreguntaIncorrecta = 0;
    this.resultado = [];
    if (Cuestionario.identityResult.length != undefined) {
      Cuestionario.identityResult.forEach(element => {
        if (element.IdRespuesta == 1) {
          this.contadorPreguntaCorrecta = this.contadorPreguntaCorrecta + 1;
        } else {
          this.contadorPreguntaIncorrecta = this.contadorPreguntaIncorrecta + 1;
        }
      });
    }
    this.resultCalificacion = (this.contadorPreguntaCorrecta) * 20 / (this.cantidadPregunta);
    localStorage.setItem('#nI', (this.numeroIntento).toString());
    Cuestionario.startCalification(true);
    Cuestionario.startResultCalificacion((this.contadorPreguntaCorrecta) * 20 / (this.cantidadPregunta));
  }

  iniciar(mostrar) {
    this.mostrarInicio = mostrar;
  }

  public cargarBalotario(cantidadPregunta): void {
    if (Cuestionario.identityReloaded && Cuestionario.identityReloaded !== false) {
      this.balotarioService.obtenerListadoPreguntaRespuesta(cantidadPregunta,Session.identity.id_balotario).subscribe(
        (WsResponse: WsResponse) => {
          if (WsResponse.codResultado == 1) {
            this.cronometro = true;
            localStorage.setItem('chr', (this.cronometro).toString());
            this.totalPregunta = (WsResponse.total != 0) ? WsResponse.total : 0;
            this.porcentaje = 100 / this.totalPregunta;
            this.pregunta = (WsResponse.response != null) ? WsResponse.response : [];
            this.iteracion = 0;
            this.numeroPregunta = 1;
            this.numeroIntento = +((localStorage.getItem('#nI') == null) ? 1 : localStorage.getItem('#nI'));
            Cuestionario.start(this.pregunta, this.cantidadIntento);
            Cuestionario.startReloaded(true);
          } else { }
        },
        error => {
          console.error(error);
        }
      );

    } else {
      this.cronometro = true;
      this.iteracion = Cuestionario.identityIteration;
      this.pregunta = Cuestionario.identityQuestion;
      this.totalPregunta = Cuestionario.identityQuestion.length;
      this.numeroPregunta = Cuestionario.identityNumberQuestion;
      this.porcentaje = this.numeroPregunta * 100 / this.totalPregunta;
      this.numeroIntento = +((localStorage.getItem('#nI') == null) ? 1 : localStorage.getItem('#nI'));
      if (Cuestionario.identityCalification == true) {
        this.calificacion = true;
      }
    }

  }

  continuar() {

    if (this.numeroPregunta <= this.totalPregunta) {
      if (Cuestionario.identityResult.length == undefined) {
        this.resultado.push({
          IdPregunta: this.idPregunta,
          IdRespuesta: ((this.estadoCorrecto) ? '1' : '0')
        });
      } else {
        this.resultado = Cuestionario.identityResult;
        this.resultado.push({
          IdPregunta: this.idPregunta,
          IdRespuesta: ((this.estadoCorrecto) ? '1' : '0')
        });
      }
      Cuestionario.startResultado(this.resultado);
      // Finalizar !!!
      if (this.numeroPregunta == this.totalPregunta) {
        this.calificacion = true;
        this.contadorPreguntaCorrecta = 0;
        this.contadorPreguntaIncorrecta = 0;
        this.resultado = [];
        Cuestionario.identityResult.forEach(element => {
          if (element.IdRespuesta == 1) {
            this.contadorPreguntaCorrecta = this.contadorPreguntaCorrecta + 1;
          } else {
            this.contadorPreguntaIncorrecta = this.contadorPreguntaIncorrecta + 1;
          }
        });
        this.resultCalificacion = (this.contadorPreguntaCorrecta) * 20 / (this.contadorPreguntaCorrecta + this.contadorPreguntaIncorrecta);
        localStorage.setItem('#nI', (this.numeroIntento).toString());
        Cuestionario.startCalification(true);
        Cuestionario.startResultCalificacion((this.contadorPreguntaCorrecta) * 20 / (this.contadorPreguntaCorrecta + this.contadorPreguntaIncorrecta));
      }
    }

    if (this.numeroPregunta < this.totalPregunta) {
      this.bloqueo = true;
      this.numeroPregunta = this.numeroPregunta + 1;
      this.iteracion = this.iteracion + 1;
      Cuestionario.startReloaded(false);
      Cuestionario.startIteration(this.iteracion);
      Cuestionario.startQuestion(this.numeroPregunta);
      this.porcentaje = this.numeroPregunta * 100 / this.totalPregunta;
    }

  }

}
