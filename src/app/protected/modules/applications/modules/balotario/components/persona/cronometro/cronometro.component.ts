import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-cronometro',
  templateUrl: './cronometro.component.html',
  styleUrls: ['./cronometro.component.scss']
})
export class CronometroComponent implements OnInit {

  @Input() tiempo: string;
  @Output() terminaTiempo = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  terminoTiempo() {
    this.terminaTiempo.emit(true);
  }

}
