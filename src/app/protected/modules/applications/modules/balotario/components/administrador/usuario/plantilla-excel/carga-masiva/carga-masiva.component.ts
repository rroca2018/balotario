import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { MENSAJES } from 'app/common';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatTableDataSource, MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ItemBean } from '../../../../../dto/ItemBean';
import { ConfirmMessageComponent } from '@shared/components/confirm-message/confirm-message.component';
import { InfoMessageComponent } from '@shared/components/info-message/info-message.component';
import { ArchivoRequest } from '../../../../../dto/request/ArchivoRequest';
import { ExcelService } from '../../../../../service/excel.service';
import { NgxSpinnerService } from "ngx-spinner";
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-carga-masiva',
  templateUrl: './carga-masiva.component.html',
  styleUrls: ['./carga-masiva.component.scss']
})
export class CargaMasivaComponent implements OnInit {

  @ViewChild('myInput')
  myInputVariable: ElementRef;

  //isLoading: boolean;

  selectedFiles: boolean;
  btnEliminar: boolean;
  examinarFiles: boolean;
  archivo: string;
  fileUpload: File;
  fileUploadAux: File;
  archivoForm: FormGroup;
  dataItem: any;
  selectedDefault: string = '2';
  archivoSeleccionado: boolean;

  fileUploadRes = { status: '', message: '', messageAux: '' };
  error: string;
  //seleccionTipoDoc= '001';
  mensaje: any;
  // Tabla

  //archivoResponse: ArchivoResponse[];
  total: number;


  columnas: string[];

  tipoDocArchivo: ItemBean[];
  tipoDocArchivoAux: ItemBean;


  dialogRefVariable: MatDialogRef<any>;
  
  dialogRefMessage: MatDialogRef<any>;
  srcResult: any;

  constructor(public dialogRef: MatDialogRef<CargaMasivaComponent>,
    private dialog: MatDialog,
    private excelService: ExcelService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService) {}

  ngOnInit() {
    //this.customIconService.cargaIcono();

  
   // this.crearFormulario();

    this.fileUploadRes.message = '20';
    this.archivo = 'Ningún archivo seleccionado'
    this.examinarFiles = false;
  }

  crearFormulario(): void {
    this.archivoForm = this.formBuilder.group({
      descArchivoFrmCtrl: [''],
      tipoDocArchivoFrmCtrl: ['']
    });
  }


  reiniciar() {
    //this.archivoForm.reset('');
    this.archivo = 'Ningún archivo seleccionado';
    //this.filtrosForm.get(tipo).setValue(null);
    // this.filtrosProyectoRequest = new ProyectoRequest();
  }

  eventAux: any;

  selectFile(event) {

    console.log(event);
    setTimeout(() => {
      this.selectedFiles = true;
    }, 800)// 1 segundo = 1000
    if( this.fileUpload == null){
      this.fileUpload = event.target.files[0];
      this.fileUploadAux =  this.fileUpload;
      this.archivo = event.target.files[0].name;
      
    }else{// Para controlar si ya existe un archivo seleccionado pero abre de nuevo el explorador y no selecciona ningún archivo.
      this.fileUpload = event.target.files[0];
      if(typeof this.fileUpload == "undefined" || this.fileUpload == null){
        this.fileUpload = this.fileUploadAux;
        this.archivo = this.fileUpload.name;
      }else{
        this.fileUpload = event.target.files[0];
        this.fileUploadAux =  this.fileUpload;
        this.archivo = event.target.files[0].name;
      }
    }
    
  }

  reset() {
    //Elimina el archivo seleccionado cuando se abre el explorador para elegir el archivo
    console.log(this.myInputVariable.nativeElement.files);
    this.myInputVariable.nativeElement.value = "";
    console.log(this.myInputVariable.nativeElement.files);
  }

  onFileSelected() {

    const inputNode: any = document.querySelector('#idFile');

    if (typeof (FileReader) !== 'undefined') {
      const reader = new FileReader();

      reader.onload = (e: any) => {
        this.srcResult = e.target.result;
      };

      reader.readAsArrayBuffer(inputNode.files[0]);
    }
  }




  public openDialogMensajeConfirm(message: string, confirmacion: boolean): void {

    this.dialogRefMessage = this.dialog.open(InfoMessageComponent, {
      width: '400px',
      disableClose: true,
      data: { message: message, confirmacion: confirmacion }
    });
  }


  validarSubidaArchivo($event) {
    $event.preventDefault();
    if (this.archivoForm.get('tipoDocArchivoFrmCtrl').value == '' || (this.archivoForm.get('tipoDocArchivoFrmCtrl').value == null)) {
      this.openDialogMensaje(MENSAJES.ARCHIVO_TITLE, MENSAJES.ARCHIVO_TIPO_DOC_REQUERIDO, true, false, false);
      return false;
    } else {
      this.upload();
    }
  }






  // carga de archivo original
  subirArchivo() {
   /* const archivoRequest: ArchivoRequest = new ArchivoRequest();
    archivoRequest.nomArchivo = this.fileUpload.name;
    archivoRequest.archivo = this.fileUpload;
    archivoRequest.idProyecto = this.datos.idProyecto + "";
    archivoRequest.descripcion = this.archivoForm.get('descArchivoFrmCtrl').value;
    archivoRequest.tipoDoc = this.archivoForm.get('tipoDocArchivoFrmCtrl').value.cidCodigo;

    this.selectedFiles = false;
    this.btnEliminar = true;
    this.examinarFiles = true;
*/
    
/*
    this.proyectoService.subirArchivo(archivoRequest).subscribe(
      (response: WsApiOutResponse) => {

        if (response.codResultado == 1) {
          console.log('carga exitosa');
          this.mensaje = MENSAJES.ARCHIVO_INFO_SUCCESS;
          this.openDialogMensaje(MENSAJES.ARCHIVO_TITLE, null, this.mensaje, false, false);
          this.listadoArchivo();
          this.reiniciar();
          this.selectedFiles = false;
          this.btnEliminar = false;
          this.examinarFiles = false;

        }

      }, error => {
        this.mensaje = MENSAJES.ERROR_SERVICIO;
        this.openDialogMensaje(MENSAJES.ARCHIVO_TITLE, this.mensaje, null, true, false);
        console.error(error);
        this.selectedFiles = true;
        this.btnEliminar = true;
      });*/
  }


  public openDialogMensaje(
    message: string,
    message2: string,
    alerta: boolean,
    confirmacion: boolean,
    valor: any
  ): void {
    const dialogRef = this.dialog.open(InfoMessageComponent, {
      width: '400px',
      disableClose: true,
      data: {
        title: MENSAJES.ARCHIVO_TITLE,
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe((ok: number) => {
      if (ok == 0) {

      }
    });
  }


  public openDialogMensajeExcelPrueba(
    message: string,
    message2: string,
    alerta: boolean,
    confirmacion: boolean,
    valor: any
  ): void {
    const dialogRef = this.dialog.open(InfoMessageComponent, {
      width: '400px',
      disableClose: true,
      data: {
        title: MENSAJES.ARCHIVO_TITLE,
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    }); debugger
    dialogRef.afterClosed().subscribe((ok: number) => {
      debugger;
     
      if (ok == 0) {
        this.dialogRef.close(ok);
      }
    });
   
  }

  public openDialogMensajeExcel(
    message: string,
    message2: string,
    alerta: boolean,
    confirmacion: boolean,
    valor: any
  ): void {
    const dialogRef = this.dialog.open(InfoMessageComponent, {
      width: '400px',
      disableClose: true,
      data: {
        title: MENSAJES.EXCEL_TITLE_ERROR,
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe((ok: number) => {
      if (ok == 0) {

      }
    });
  }

  upload() {
  /*  const archivoRequest: ArchivoRequest = new ArchivoRequest();
    archivoRequest.nomArchivo = this.fileUpload.name;
    archivoRequest.archivo = this.fileUpload;
    archivoRequest.idProyecto = this.datos.idProyecto + "";
    archivoRequest.fidProyecto = this.datos.fidProyecto;
    archivoRequest.descripcion = this.archivoForm.get('descArchivoFrmCtrl').value;
    this.tipoDocArchivoAux = this.archivoForm.get('tipoDocArchivoFrmCtrl').value;*/

    this.selectedFiles = false;
    this.btnEliminar = true;
    this.examinarFiles = true;

    
      //this.subirArchivo();
    


  }



/*
  validarDocumentoExcel(fidProyecto: number, tipoDoc: number, archivoRequest: ArchivoRequest, cidCodigoTipoDoc: string) {

    document.querySelector('#idFile').nodeValue = "";
    const inputNode: any = document.querySelector('#idFile').nodeValue = "";
    this.proyectoService.validarDocumentoExcel(fidProyecto, tipoDoc).subscribe(
      (response: WsApiOutResponse) => {

        if (response.codResultado == 1) {
          if (cidCodigoTipoDoc == '007') {
            this.cargarExcelPresupuesto(archivoRequest);

          } else if (cidCodigoTipoDoc == '008') {
            this.cargarExcelPartida(archivoRequest);

          } else if (cidCodigoTipoDoc == '009') {
            this.cargarExcelGastoGeneral(archivoRequest);

          } else if (cidCodigoTipoDoc == '010') {
            this.cargarExcelGastoSupervicion(archivoRequest);
          }
          this.reiniciar();
          this.selectedFiles = false;
          this.btnEliminar = false;
          this.examinarFiles = false;
        } else if (response.codResultado == 0) {

          this.reset();
          this.archivo = "";
          this.mensaje = response.msgResultado;
          this.openDialogMensaje("", this.mensaje, true, false, "error_validacion");

          this.selectedFiles = false;
          this.btnEliminar = false;
          this.examinarFiles = false;

        }

      }, error => {
        this.mensaje = MENSAJES.ERROR_SERVICIO;
        this.openDialogMensaje(MENSAJES.ARCHIVO_TITLE, this.mensaje, true, false, null);
        console.error(error);
        this.selectedFiles = true;
        this.btnEliminar = true;

      });


  }*/

  cargarExcelPresupuesto(event) {

    const archivoRequest: ArchivoRequest = new ArchivoRequest();
    archivoRequest.nomArchivo = this.fileUpload.name;
    archivoRequest.archivo = this.fileUpload;

    //this.proyectoService.cargarExcelPresupuesto(archivoRequest).pipe(delay(2000)).subscribe(
    this.excelService.cargarExcel(archivoRequest).subscribe(
      (response: any) => {

        if (response.codResultado == 1) {
          console.log('carga exitosa');
          this.mensaje = MENSAJES.ARCHIVO_INFO_SUCCESS;
          this.openDialogMensajeExcelPrueba("", this.mensaje, true, false, null);
        
       
          this.reset();
          this.reiniciar();
          this.selectedFiles = false;
          this.btnEliminar = false;
          this.examinarFiles = false;


        } else if (response.codResultado == 0) {
 
          debugger;
          console.log('No se encontro el dato del excel en la bd');
          console.log(response);

          console.log(response.response.value);

          //this.cargarExcelPartidaTxtError("listaErroresPresupuesto");
          this.openDialogMensajeExcel(null, response.response.message + " (Fila " + response.response.nroFila + ", " + "Columna " + response.response.nroColumna +")" , true, false, null);
          //this.listadoArchivo();
          this.reset();
          this.reiniciar();
          this.selectedFiles = false;
          this.btnEliminar = false;
          this.examinarFiles = false;

        } else if (response.codResultado == 2) {
          console.log('carga errorea');
          this.openDialogMensaje(null, response.msgResultado, true, false, null);
          this.reset();
          this.reiniciar();
          this.selectedFiles = false;
          this.btnEliminar = false;
          this.examinarFiles = false;

        }
 

      }, error => {

        this.mensaje = MENSAJES.ERROR_SERVICIO;
        this.openDialogMensaje(MENSAJES.ARCHIVO_TITLE, this.mensaje, true, false, null);
        this.reset();
        this.reiniciar();
        console.error(error);
        this.selectedFiles = true;
        this.btnEliminar = true;

      });

  }


  cargarExcelPartidaTxtError(errorPartida: string) {
    this.excelService.cargarExcelPartidaTxtError(errorPartida);
  }


  cancelarCarga(){
    this.openDialogMensajeConfirm(MENSAJES.EXCEL_CANCELAR_CARGA, true);
    this.dialogRefMessage.afterClosed()
    .pipe(filter(verdadero => !!verdadero))
    .subscribe(() => {   
      this.dialogRef.close(true);
    });
  }
  

}

