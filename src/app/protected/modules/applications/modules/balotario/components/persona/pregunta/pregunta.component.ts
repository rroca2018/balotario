import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { preguntaResponse } from '../../../dto/preguntaResponse';
import { respuestaResponse } from '../../../dto/respuestaResponse';

@Component({
  selector: 'app-pregunta',
  templateUrl: './pregunta.component.html',
  styleUrls: ['./pregunta.component.scss'],
})
export class PreguntaComponent implements OnInit {

  @Input() pregunta: preguntaResponse[];
  @Input() iteracion: number;
  @Output() responde = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  responder(idRespuesta: number, idPregunta: number, idRespuestaCorrecto: number) {
    const cuestionario = {
      idPregunta: idPregunta,
      idRespuesta: idRespuesta,
      idRespuestaCorrecto: idRespuestaCorrecto,
      correcto: ((idRespuestaCorrecto == idRespuesta) ? true : false),
      estado: false
    }
    this.responde.emit(cuestionario);
  }

}
