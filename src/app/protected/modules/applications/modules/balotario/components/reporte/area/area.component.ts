import { Component, OnInit, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import { BalotarioService } from '../../../service/balotario.service';
import { WsResponse } from '../../../dto/WsResponse';
import { MatTableDataSource, MatPaginator, PageEvent } from '@angular/material';

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.scss']
})
export class AreaComponent implements OnInit {

  // TABLA
  pagina = 1;
  cantidad = 2;
  total = 0;

  dataSource: MatTableDataSource<any>;
  WsResponse: WsResponse;
  proyectoResponse: any[];
  objproyectoResponse: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  isLoading: boolean;
  columnas: string[] = [];

  // CHART
  Estado = [];
  Porcentaje = [];
  chart = [];
  constructor(
    private balotarioService: BalotarioService
  ) {
    this.dataSource = new MatTableDataSource([]);
  }

  ngOnInit() {
    this.obtenerChart();
    this.generarCabeceraColumnas();
    this.cargarArea();
  }

  public cargarTablaArea(): void {
    if (this.proyectoResponse != null && this.proyectoResponse.length > 0) {
      this.dataSource = new MatTableDataSource(this.proyectoResponse);
    }
  }

  public cargarArea(): void {
    this.dataSource = null;
    this.proyectoResponse = [];
    this.isLoading = true;
    this.balotarioService.listadoArea(this.pagina, this.cantidad/*, this.filtrosTrabajadorRequest*/).subscribe(
        (WsResponse: WsResponse) => {
          if (WsResponse.codResultado == 1) {
            this.proyectoResponse = (WsResponse.response != null) ? WsResponse.response : [];
            this.total = (WsResponse.total != 0) ? WsResponse.total : 0;
            this.cargarTablaArea();
          } else {
          }
          this.isLoading = false;
        },
        error => {
          console.error(error);
        }
      );
  }

  generarCabeceraColumnas(): void {
    this.columnas = [
      'nro',
      'area',
      'evaluacion',
      'avance'
    ];
  }

  cambiarPagina(event: PageEvent) {
    this.pagina = event.pageIndex + 1;
    this.cantidad = event.pageSize;
    this.cargarArea();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  obtenerChart() {
    this.balotarioService.listado().subscribe((WsResponse: WsResponse) => {
      WsResponse.response.forEach(x => {
        this.Estado.push(x.estado);
        this.Porcentaje.push(x.porcentaje);
      });
      this.chart = new Chart('canvas', {
        type: 'pie',
        data: {
          labels: this.Estado,
          datasets: [
            {
              data: this.Porcentaje,
              backgroundColor: [
                "#3cb371",
                "#D7DF01",
                "#DF0101"
              ],
              fill: true
            }
          ]
        },
        options: {
          legend: {
            display: true,
            position: 'right',
            labels: {
              fontColor: 'rgb(0, 0, 0)',
              generateLabels: function (chart) {
                var newLabels = [];
                Chart.defaults.pie.legend.labels.generateLabels(chart).forEach((label, i) => {
                  label.text = chart.data.datasets[0].data[i] + ' ' + chart.data.labels[i];
                  newLabels.push(label)
                });
                return newLabels;
              }
            }
          },
          tooltips: {
            enabled: false
          },

        }
      });
    });
  }







}
