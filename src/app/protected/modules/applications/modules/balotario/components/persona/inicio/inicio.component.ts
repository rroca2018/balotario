import { Component, OnInit, Output, EventEmitter } from '@angular/core';

declare const startCountDown: any;

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

  @Output() muestraInicio = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  iniciar() {
    startCountDown();
    localStorage.setItem('init', (true).toString());
    this.muestraInicio.emit(true);
  }

}
