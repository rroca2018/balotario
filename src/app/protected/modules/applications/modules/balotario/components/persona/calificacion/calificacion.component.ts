import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Cuestionario } from '../../../dto/Cuestionario';

@Component({
  selector: 'app-calificacion',
  templateUrl: './calificacion.component.html',
  styleUrls: ['./calificacion.component.scss']
})
export class CalificacionComponent implements OnInit {

  @Input() resultCalificacion: number;
  @Input() cantidadIntento: number;
  @Input() numeroIntento: number;

  @Output() intenta = new EventEmitter();
  @Output() finaliza = new EventEmitter();

  CresultCalificacion: number;
  CcantidadIntento: number;
  CnumeroIntento: number;

  cantidad: number;
  ocultar: boolean = false;

  constructor() { }

  ngOnInit() {
    if (Cuestionario.identityCalification == true) {
      (Cuestionario.identityResultCalificacion) ? this.CresultCalificacion = Cuestionario.identityResultCalificacion : this.CresultCalificacion = this.resultCalificacion;
      (Cuestionario.identityTotalAttempt) ? this.CcantidadIntento = Cuestionario.identityTotalAttempt : this.CcantidadIntento = this.cantidadIntento;
      (localStorage.getItem('#nI')) ? this.CnumeroIntento = +localStorage.getItem('#nI') : this.CnumeroIntento = this.numeroIntento;
      
      if (Cuestionario.detenerCuestionario == 2) {
        // localStorage.setItem('dt', (true).toString());
        this.ocultar = true;
      } else {
        // localStorage.setItem('dt', (false).toString());
      }
      
      if (this.CnumeroIntento == this.CcantidadIntento) {
        this.ocultar = true;
      }

    }
  }

  intentarNuevo() {
    this.cantidad = +localStorage.getItem('#nI');
    localStorage.setItem('#nI', (this.cantidad + 1).toString());
    this.intenta.emit(null);
  }

  finalizar() {
    this.finaliza.emit();
  }



}

