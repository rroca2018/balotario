import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BalotarioRoutingModule } from './balotario-routing.module';
import { SharedModule } from '@shared/shared.module';
import { AuthModule } from '@shared/auth/auth.module';
import { LoadingModule } from '@shared/loading/loading.module';
import { CountdownModule } from 'ngx-countdown';
import { ReporteComponent } from './components/reporte/reporte.component';
import { MatTabsModule } from '@angular/material';
import { AreaComponent } from './components/reporte/area/area.component';
import { EvaluadorComponent } from './components/reporte/evaluador/evaluador.component';
import { ColaboradorComponent } from './components/reporte/colaborador/colaborador.component';
import { AutoevaluacionComponent } from './components/reporte/autoevaluacion/autoevaluacion.component';
import { BalotarioService } from './service/balotario.service';
import { ResultadoComponent } from './components/resultado/resultado.component';
import { RegistrarActualizarComponent } from './components/administrador/usuario/registrar-actualizar/registrar-actualizar.component';
import { UsuarioComponent } from './components/administrador/usuario/usuario.component';
import { ItemService } from './service/item.service';
import { PersonaService } from './service/persona.service';
import { PlantillaExcelComponent } from './components/administrador/usuario/plantilla-excel/plantilla-excel.component';
import { CargaMasivaComponent } from './components/administrador/usuario/plantilla-excel/carga-masiva/carga-masiva.component';
import { ExcelService } from './service/excel.service';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [
    ReporteComponent,
    AreaComponent,
    EvaluadorComponent,
    ColaboradorComponent,
    AutoevaluacionComponent,
    ResultadoComponent,
    UsuarioComponent,
    RegistrarActualizarComponent,
    PlantillaExcelComponent,
    CargaMasivaComponent
  ],
  entryComponents: [
    RegistrarActualizarComponent,
    PlantillaExcelComponent,
    CargaMasivaComponent
  ],
  providers: [
    BalotarioService,
    ItemService,
    PersonaService,
    ExcelService
  ],
  imports: [
    SharedModule,
    CommonModule,
    BalotarioRoutingModule,
    CountdownModule,
    AuthModule.forRoot(),
    LoadingModule.forRoot(),
    MatTabsModule,
    NgxSpinnerModule
  ]
})
export class BalotarioModule { }
