import { respuestaResponse } from './respuestaResponse';

export interface preguntaResponse {
    idPregunta: number;
    nombrePregunta: string;
    idRespuestaCorrecto: number;
    respuesta: respuestaResponse[];
}