import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { WsResponse } from '../dto/WsResponse';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'environments/environment';

@Injectable()
export class BalotarioService {

  HOST: string = environment.backendUrlProj;

  constructor(
    private http: HttpClient
  ) { }

  obtenerListadoPreguntaRespuesta(cantidadPregunta: number, idBalotario: number): Observable<WsResponse> {
    return this.http.get<any>(`${this.HOST}/cuestionario/listaPregunta/${cantidadPregunta}/${idBalotario}`);
  }

  listadoArea(pagina: number, cantidad: number): Observable<WsResponse> {
    return this.http.get<WsResponse>('assets/listadoArea.json');
  }

  listado(): Observable<WsResponse> {
    return this.http.get<WsResponse>('assets/listado.json');
  }

  obtenerComboDominio(): Observable<WsResponse> {
    return this.http.get<WsResponse>('assets/comboDominio.json');
  }

}
