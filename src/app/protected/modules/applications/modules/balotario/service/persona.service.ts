

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { PersonaRequest } from '../dto/request/PersonaRequest';
import { WsResponse } from '../dto/WsResponse';

@Injectable()
export class PersonaService {
  


  constructor(
    private http: HttpClient
  ) { }

  crearPersona(persona: PersonaRequest) {
    debugger;
    return this.http.post<PersonaRequest>(`${environment.backendUrlProj}/persona/registrarPersona`, persona);
  }


  listarPersonas(): Promise<any> {
    return this.http.get<any>(`${environment.backendUrlProj}/persona/listarPersonas`).toPromise(); 
  }

  obtenerPersona(idUsuario: string) {
    return this.http.get<any>(`${environment.backendUrlProj}/persona/obtenerPersona/${idUsuario}`); 
  }
  

  actualizarPersona(persona: PersonaRequest) {
    debugger;
    return this.http.post<PersonaRequest>(`${environment.backendUrlProj}/persona/actualizarPersona`, persona);
  }


  verificarUsuario(usuario: String): Promise<WsResponse>{
    //return this.http.post<any>(`${environment.backendUrlProj}/persona/verificarUsuario`, usuario);
    return this.http.post<WsResponse>(`${environment.backendUrlProj}/persona/verificarUsuario`, usuario).toPromise();
  }


  verificarCodTrabajador(verificarRequest): Promise<WsResponse>{
    return this.http.post<WsResponse>(`${environment.backendUrlProj}/persona/verificarCodigoTrabajador`, verificarRequest).toPromise();
  }

  verificarNumDocumento(verificarRequest): Promise<WsResponse> {
    return this.http.post<WsResponse>(`${environment.backendUrlProj}/persona/verificarNumeroDocumento`, verificarRequest).toPromise();
  }

  buscarPersonas(textoBuscar: String): any {
    return this.http.post<any>(`${environment.backendUrlProj}/persona/buscarPersonas`, textoBuscar); 
  }

  downloadFile() { 
    const REQUEST_URI = `${environment.backendUrlProj}/archivo/descargarArchivo`;
    return this.http.get(REQUEST_URI, {
      responseType: 'arraybuffer'
    })
  }

/*
  roles(opcionUrl: string): Promise<Rol[]> {
    return this.http.get<Rol[]>(`${this.urlBase}/roles?rutaOpcion=${opcionUrl}`).toPromise();
  }*/

}