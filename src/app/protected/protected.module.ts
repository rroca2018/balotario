import { NgModule, ModuleWithProviders } from '@angular/core';
import { ProtectedComponent } from './protected.component';
import { ProtectedRoutingModule } from './protected-routing.module';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { HomeComponent } from './components/home/home.component';
import { AuthService } from './services/auth.service';
import { environment } from 'environments/environment';

import {
  MatFormFieldModule,
  MatInputModule,
  MatCardModule,
  MatSidenavModule,
  MatButtonModule,
  MatTreeModule,
  MatIconModule,
  MatTooltipModule,
  MatMenuModule,
  MatDialogModule,
  MatProgressBarModule,
  MatRadioModule
} from '@angular/material';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthModule } from '@shared/auth/auth.module';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { LoadingModule } from '@shared/loading/loading.module';
import { PersonaComponent } from './modules/applications/modules/balotario/components/persona/persona.component';
import { PreguntaComponent } from './modules/applications/modules/balotario/components/persona/pregunta/pregunta.component';
import { CalificacionComponent } from './modules/applications/modules/balotario/components/persona/calificacion/calificacion.component';
import { CronometroComponent } from './modules/applications/modules/balotario/components/persona/cronometro/cronometro.component';
import { InicioComponent } from './modules/applications/modules/balotario/components/persona/inicio/inicio.component';
import { CountdownModule } from 'ngx-countdown';
import { AdministradorComponent } from './modules/applications/modules/balotario/components/administrador/administrador.component';

@NgModule({
  declarations: [
    ProtectedComponent,
    NotFoundComponent,
    HomeComponent,
    NavbarComponent,
    ToolbarComponent,
    //cuestionario
    InicioComponent, 
    PreguntaComponent,
    CalificacionComponent,
    CronometroComponent,
    InicioComponent,
    PersonaComponent,
    //administrador
    AdministradorComponent
    
  ],
  imports: [
    ProtectedRoutingModule,
    CommonModule,
    HttpClientModule,
    AuthModule.forRoot(),
    LoadingModule.forRoot(),
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatSidenavModule,
    MatButtonModule,
    MatTreeModule, 
    MatIconModule,
    MatTooltipModule,
    MatMenuModule,
    MatDialogModule,
    MatTooltipModule,
    MatProgressBarModule,
    MatRadioModule,
    CountdownModule
  ],
  providers: [
    AuthService
  ],
  entryComponents: [
    
  ]
})
export class ProtectedModule {   
 /*  static forRoot(): ModuleWithProviders {
  return {
    ngModule: LoadingModule,
    providers: [
      {
        provide: HTTP_INTERCEPTORS,
        useClass: null,
        multi: true
      }
    ]
  };
} */
}
