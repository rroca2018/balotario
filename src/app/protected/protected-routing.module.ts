import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProtectedComponent } from './protected.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AuthRouting } from '@shared/auth/auth-routing';
import { PersonaComponent } from './modules/applications/modules/balotario/components/persona/persona.component';
import { AdministradorComponent } from './modules/applications/modules/balotario/components/administrador/administrador.component';


const routes: Routes = [
  {
    path: '',
    component: ProtectedComponent,
    children: [
      {
        path: 'principal',
        children: [
          { path: 'administrador', component: AdministradorComponent, canActivate: [AuthRouting] },
          { path: 'persona', component: PersonaComponent, canActivate: [AuthRouting] },
        ]
      },
      {
        path: 'seguridad',
        loadChildren: './modules/security/security.module#SecurityModule'
      },
      {
        path: 'applications',
        loadChildren: './modules/applications/applications.module#ApplicationsModule'
      },
      { path: '', redirectTo: 'principal/administrador', pathMatch: 'full' }
    ],
  },
  { path: '404', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProtectedRoutingModule { }
