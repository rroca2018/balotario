import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { Session } from '@shared/auth/Session';
import { Constante } from 'app/protected/modules/applications/modules/balotario/dto/Constante';

@Component({
  selector: 'public-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  authForm: FormGroup; 

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.authForm = this.formBuilder.group({
      username: ['87654321'],// Validators.required],
      password: ['12345']// Validators.required]
    });
  }

  onSubmit() {
    if (this.authForm.valid) {
      this.authService.login(
        this.authForm.value
      ).subscribe(
        (session) => {
          debugger;
          Session.start(session);
          console.log(Session.identity);
          if (Session.identity.puesto == Constante.ADMINISTRADOR) {
            this.router.navigate(['/principal/administrador']);
          } else {
            //inicio del cuestionario
            this.router.navigate(['/principal/persona']);
          }
        },
        () => {
          this.authForm.get('username').setErrors({ resp: 'Usuario o contraseña incorrecto.' });
        }
      );
    } else {
      Object.values(this.authForm.controls).forEach(c => c.markAsTouched());
    }
  }

}
